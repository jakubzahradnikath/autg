﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Generowanie_grafu
{
    public class Form1Class
    {
        private static int a, b;
        private static int[,] Matrix;
        private static double[,] kordy;
        private static double stopnie;
        private static double radian;
        private static int[] S; // Stos w tablicy
        private static int[] D; // Tablica numerów wierzchołków
        private static int cv;
        private static int sptr = 0;

        public static void button1Function(TextBox textBox1, TextBox textBox2, TextBox textBox3, Bitmap bmap, Graphics g, Pen p, PictureBox pictureBox1, Label label1)
        {
            // a - wierzchołki

            try
            {
                a = Convert.ToInt32(textBox1.Text);
                b = Convert.ToInt32(textBox3.Text);
                Matrix = new int[a, a];

                textBox2.Text = "";

                GenerujGraf();
                WypelnijMacierz(textBox2);

                kordy = new double[a, 2];
                stopnie = 360 / a;
                radian = (Math.PI * stopnie / 180);
                bmap = new Bitmap(500, 500);
                g = Graphics.FromImage(bmap);

                RysujGraf(g, textBox2, p);
                Pen p2 = new Pen(Color.Black, 4);
                RysujTrase(g, p2);
                findEuler(0);

                RysujTrase(g, new Pen(Color.Red, 4));
                pictureBox1.Image = new Bitmap(bmap, pictureBox1.Width, pictureBox1.Height);
            }
            catch (FormatException)
            {
                textBox1.Text = "LICZBA SUKO";
            }
        }

        private static void GenerujGraf()
        {
            Random random = new Random();
            int r1;
            // GENERUJ GRAF EULERA
            // wyczyść

            for (int i = 0; i < a; i++)
            {
                for (int j = 0; j < a; j++)
                {
                    Matrix[i, j] = 0;
                }
            }

            //generate graph with given a and b
            for (int i = 1; i < a; i++)
            {
                for (int j = 0; j < i; j++)
                {
                    if ((r1 = random.Next(0, 100)) < b)
                    {
                        Matrix[i, j] = 1;
                        Matrix[j, i] = 1;
                    }
                }
            }

            //modify to have EULER
            for (int i = 0; i < a - 1; i++)
            {
                //calculate degree
                int deg = 0;
                for (int j = 0; j < a; j++)
                {
                    if (Matrix[i, j] > 0)
                    {
                        deg++;
                    }
                }
                //check if degree is even
                if (deg % 2 != 0)
                {
                    int x = random.Next(a - i - 1) + i + 1;
                    if (Matrix[i, x] > 0)
                    {
                        Matrix[i, x] = 0;
                        Matrix[x, i] = 0;
                    }
                    else
                    {
                        Matrix[i, x] = 1;
                        Matrix[x, i] = 1;
                    }
                }
            }
        }

        private static void RysujGraf(Graphics g, TextBox textBox2, Pen p)
        {
            Font font1 = new Font("Arial", 20);

            // rysowanie kółek na planszy
            for (int i = 0; i < a; i++)
            {
                kordy[i, 0] = 200 * Math.Cos(radian * i);
                kordy[i, 1] = 200 * Math.Sin(radian * i);
                g.DrawEllipse(p, new Rectangle(Convert.ToInt32(kordy[i, 0]) + 250, Convert.ToInt32(kordy[i, 1]) + 250, 30, 30));
                //dodanie numeru kółka
                g.DrawString(i.ToString(), font1, Brushes.Red, new Point(Convert.ToInt32(kordy[i, 0]) + 250, Convert.ToInt32(kordy[i, 1]) + 250));

                //Textbox1.Text += kordy[i, 0] + ".. " + kordy[i, 1]; ;
                textBox2.Text += radian * i;
                textBox2.Text += System.Environment.NewLine;
            }

            // KONIEC rysowanie kółek na planszy
        }

        private static void RysujTrase(Graphics g, Pen p2)
        {
            // rysowanie trasy według Matrix[,]

            for (int i = 0; i < a; i++)
            {
                for (int j = 0; j < a; j++)
                {
                    if (Matrix[i, j] == 1)
                    {
                        g.DrawLine(p2, Convert.ToInt32(kordy[i, 0]) + 260, Convert.ToInt32(kordy[i, 1]) + 260,
                            Convert.ToInt32(kordy[j, 0]) + 260, Convert.ToInt32(kordy[j, 1]) + 260);
                    }
                }
            }
        }

        private static void WypelnijMacierz(TextBox textBox2)
        {
            for (int i = 0; i < a; i++)
            {
                for (int j = 0; j < a; j++)
                {
                    textBox2.Text += Matrix[i, j].ToString();
                    textBox2.Text += ' ';
                }
                textBox2.Text += Environment.NewLine;
            }
        }

        private static int DFSb(int v, int vf)
        {
            int Low;
            int temp;
            int i;

            D[v] = cv; // Numerujemy wierzchołek
            Low = cv; // Wstępna wartość Low
            cv++; // Numer dla następnego wierzchołka
            for (i = 0; i < a; i++) // Przeglądamy sąsiadów v
            {
                if ((Matrix[v, i] != vf) && (i != vf))
                {
                    if (D[i] == 0) // Jeśli sąsiad nieodwiedzony, to
                    {
                        temp = DFSb(i, v); // to wywołujemy rekurencyjnie DFSb()
                        if (temp < Low)
                        {
                            Low = temp; // Modyfikujemy Low
                        }
                    }
                    else if (D[i] < Low)
                    {
                        Low = D[i];
                    }
                }
            }

            if ((vf > -1) && (Low == D[v])) // Mamy most?
            {
                Matrix[vf, v] = Matrix[v, vf] = 2; // Oznaczamy krawędź vf-v jako most
            }

            return Low;
        }

        // Procedura wyszukuje cykl lub ścieżkę Eulera
        // We:
        // v - wierzchołek startowy
        //--------------------------------------------

        private static void findEuler(int v)
        {
            int u;
            int w;
            int i;
            S = new int[a * a * a];
            D = new int[a * a * a];
            while (true) // W pętli przetwarzamy graf
            {
                S[sptr++] = v; // v umieszczamy na stosie

                for (u = 0; (u < a); u++)
                {
                    ; // Szukamy pierwszego sąsiada v
                }

                if (u == a)
                {
                    break; // Nie ma sąsiadów, kończymy
                }

                for (i = 0; i < a; i++)
                {
                    D[i] = 0; // Zerujemy tablicę D
                }

                cv = 1; // Numer pierwszego wierzchołka dla DFS
                DFSb(v, -1); // Identyfikujemy krawędzie-mosty

                // Szukamy krawędzi nie będącej mostem

                for (w = u + 1; (Matrix[v, u] == 2) && (w < a); w++)
                {
                    u = w;
                }

                Matrix[v, u] = Matrix[u, v] = 0; // Usuwamy krawędź v-u
                v = u; // Przechodzimy do u
            }
        }
    }
}