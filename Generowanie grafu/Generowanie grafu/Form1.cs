﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Generowanie_grafu
{
    public partial class Form1 : Form
    {
        public Bitmap bmap;
        public Graphics g;
        public Pen p = new Pen(Color.Black, 2);

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form1Class.button1Function(textBox1, textBox2, textBox3, bmap, g, p, pictureBox1, label1);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }
    }
}